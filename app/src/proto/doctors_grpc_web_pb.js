/**
 * @fileoverview gRPC-Web generated client stub for proto
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('./google/api/annotations_pb.js')
const proto = {};
proto.proto = require('./doctors_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.proto.DoctorServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.proto.DoctorServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.NilMsg,
 *   !proto.proto.DoctorList>}
 */
const methodDescriptor_DoctorService_ListDoctors = new grpc.web.MethodDescriptor(
  '/proto.DoctorService/ListDoctors',
  grpc.web.MethodType.UNARY,
  proto.proto.NilMsg,
  proto.proto.DoctorList,
  /**
   * @param {!proto.proto.NilMsg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.DoctorList.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.NilMsg,
 *   !proto.proto.DoctorList>}
 */
const methodInfo_DoctorService_ListDoctors = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.DoctorList,
  /**
   * @param {!proto.proto.NilMsg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.DoctorList.deserializeBinary
);


/**
 * @param {!proto.proto.NilMsg} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.proto.DoctorList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.proto.DoctorList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.proto.DoctorServiceClient.prototype.listDoctors =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/proto.DoctorService/ListDoctors',
      request,
      metadata || {},
      methodDescriptor_DoctorService_ListDoctors,
      callback);
};


/**
 * @param {!proto.proto.NilMsg} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.proto.DoctorList>}
 *     Promise that resolves to the response
 */
proto.proto.DoctorServicePromiseClient.prototype.listDoctors =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/proto.DoctorService/ListDoctors',
      request,
      metadata || {},
      methodDescriptor_DoctorService_ListDoctors);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.NilMsg,
 *   !proto.proto.Doctor>}
 */
const methodDescriptor_DoctorService_ListDoctorsStream = new grpc.web.MethodDescriptor(
  '/proto.DoctorService/ListDoctorsStream',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.proto.NilMsg,
  proto.proto.Doctor,
  /**
   * @param {!proto.proto.NilMsg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.Doctor.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.NilMsg,
 *   !proto.proto.Doctor>}
 */
const methodInfo_DoctorService_ListDoctorsStream = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.Doctor,
  /**
   * @param {!proto.proto.NilMsg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.Doctor.deserializeBinary
);


/**
 * @param {!proto.proto.NilMsg} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.proto.Doctor>}
 *     The XHR Node Readable Stream
 */
proto.proto.DoctorServiceClient.prototype.listDoctorsStream =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/proto.DoctorService/ListDoctorsStream',
      request,
      metadata || {},
      methodDescriptor_DoctorService_ListDoctorsStream);
};


/**
 * @param {!proto.proto.NilMsg} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.proto.Doctor>}
 *     The XHR Node Readable Stream
 */
proto.proto.DoctorServicePromiseClient.prototype.listDoctorsStream =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/proto.DoctorService/ListDoctorsStream',
      request,
      metadata || {},
      methodDescriptor_DoctorService_ListDoctorsStream);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.Doctor,
 *   !proto.proto.Doctor>}
 */
const methodDescriptor_DoctorService_AddDoctor = new grpc.web.MethodDescriptor(
  '/proto.DoctorService/AddDoctor',
  grpc.web.MethodType.UNARY,
  proto.proto.Doctor,
  proto.proto.Doctor,
  /**
   * @param {!proto.proto.Doctor} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.Doctor.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.Doctor,
 *   !proto.proto.Doctor>}
 */
const methodInfo_DoctorService_AddDoctor = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.Doctor,
  /**
   * @param {!proto.proto.Doctor} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.Doctor.deserializeBinary
);


/**
 * @param {!proto.proto.Doctor} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.proto.Doctor)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.proto.Doctor>|undefined}
 *     The XHR Node Readable Stream
 */
proto.proto.DoctorServiceClient.prototype.addDoctor =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/proto.DoctorService/AddDoctor',
      request,
      metadata || {},
      methodDescriptor_DoctorService_AddDoctor,
      callback);
};


/**
 * @param {!proto.proto.Doctor} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.proto.Doctor>}
 *     Promise that resolves to the response
 */
proto.proto.DoctorServicePromiseClient.prototype.addDoctor =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/proto.DoctorService/AddDoctor',
      request,
      metadata || {},
      methodDescriptor_DoctorService_AddDoctor);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.proto.NilMsg,
 *   !proto.proto.NilMsg>}
 */
const methodDescriptor_DoctorService_Alive = new grpc.web.MethodDescriptor(
  '/proto.DoctorService/Alive',
  grpc.web.MethodType.UNARY,
  proto.proto.NilMsg,
  proto.proto.NilMsg,
  /**
   * @param {!proto.proto.NilMsg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.NilMsg.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.proto.NilMsg,
 *   !proto.proto.NilMsg>}
 */
const methodInfo_DoctorService_Alive = new grpc.web.AbstractClientBase.MethodInfo(
  proto.proto.NilMsg,
  /**
   * @param {!proto.proto.NilMsg} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.proto.NilMsg.deserializeBinary
);


/**
 * @param {!proto.proto.NilMsg} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.proto.NilMsg)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.proto.NilMsg>|undefined}
 *     The XHR Node Readable Stream
 */
proto.proto.DoctorServiceClient.prototype.alive =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/proto.DoctorService/Alive',
      request,
      metadata || {},
      methodDescriptor_DoctorService_Alive,
      callback);
};


/**
 * @param {!proto.proto.NilMsg} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.proto.NilMsg>}
 *     Promise that resolves to the response
 */
proto.proto.DoctorServicePromiseClient.prototype.alive =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/proto.DoctorService/Alive',
      request,
      metadata || {},
      methodDescriptor_DoctorService_Alive);
};


module.exports = proto.proto;

