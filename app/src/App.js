import './App.css';
import React, { Component } from 'react';

const {DoctorServiceClient} = require('./proto/doctors_grpc_web_pb.js');
const { Doctor, NilMsg } = require("./proto/doctors_pb.js");

// create client
var doctorService = new DoctorServiceClient('http://localhost:8080');

class App extends Component {

  render() {
    return (
      <div>
        <DoctorForm />
        <br/>
        <DoctorList />
        <DoctorListStream />
      </div>
    )
  }
}

class DoctorListStream extends Component {
  listDoctors() {
    const request = new NilMsg();

    // create stream for listing doctors
    let stream = doctorService.listDoctorsStream(request);

    stream.on('data', function(response) {
      console.log('got a doctor from stream! ' + response.getName() + ' ' + response.getEmail() + ' ' + response.getAge());
    });
    stream.on('status', function(status) {
      if (status.metadata) {
        console.log('grpc status');
      }
    });
    stream.on('end', function(end) {
      console.log('ending stream');
    });
  };

  render() {
    return (
      <div>{this.listDoctors()}</div>
  )}
}

function DoctorList () {
  const [doctors, updateDoctors] = React.useState([]);

  React.useEffect(function effectFunction() {
    const request = new NilMsg();
    doctorService.listDoctors(request, {},
      function(err, response) {
        if (err) {
          console.error("error getting doctors. " + err.code + ' ' + err.message);
        } else {
          updateDoctors(response.getDoctorsList());
        }
      });
  }, []);

  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Age</th>
        </tr>
      </thead>
      <tbody id='doctors'>
        {
          doctors.map(doctor => (
            <tr key={doctor.getName()}>
              <td>{doctor.getName()}</td>
              <td>{doctor.getEmail()}</td>
              <td>{doctor.getAge()}</td>
            </tr>)
          )
        }
      </tbody>
    </table>
  );
}

class DoctorForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: '',
      email: '',
      age: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    const request = new Doctor();

    request.setAge(this.state.age);
    request.setName(this.state.fullName);
    request.setEmail(this.state.email);

    const call = doctorService.addDoctor(request, {},
      (err, response) => {
        if (err) {
          console.error("error adding doctor. " + err.code + ' ' + err.message);
        } else {
          console.log('added doctor! ' + response.getName() + ' ' + response.getEmail() + ' ' + response.getAge());
        }
      });

    call.on('status', function(status) {
      if (status.metadata) {
        console.log('grpc status');
        // console.log(status.code);
        // console.log(status.details);
        // console.log(status.metadata);
      }
    });
  
    event.preventDefault();
  };

  render() {
    return (
        <form onSubmit={this.handleSubmit}>
          <h1>Doctor List</h1>

          <label>
            Name:
            <input
              name="name"
              type="name"
              value={this.state.fullName}
              onChange={e => this.setState({ fullName: e.target.value })}
              required />
          </label>

          <label>
            Email:
            <input
              name="email"
              type="email"
              value={this.state.email}
              onChange={e => this.setState({ email: e.target.value })}
              required />
          </label>

          <label>
            Age:
            <input
              name="age"
              type="number"
              value={this.state.age}
              onChange={e => this.setState({ age: e.target.value })}
              required />
          </label>

          <button>Add doctor</button>
        </form>
    )
  };
}

export default App;
