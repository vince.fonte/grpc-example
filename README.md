# grpc-example

## To run
* `docker-compose build && docker-compose up`

## Check out the OpenAPI/Swagger docs

`http://localhost:8082`

## Use the UI

`http://localhost:3000`

* Use form to add a doctor
* Refresh page to see a list of doctors
* Don't complain about how ugly it is

## Testing gRPC servers

The Go implementation is on port 50051 and the python on 50055. All the examples below are the same for either
port.

* install the `grpc_cli` program using `brew`
```
brew install grpc
```
* list all the services on the given port

```
grpc_cli ls localhost:50055 -l
```
* check the `/alive` works
```
grpc_cli call localhost:50055 Alive ""
```
* add a doctor
```
grpc_cli call localhost:50055 AddDoctor "name: 'Bob' email: 'bob@bob.com' age:98"
connecting to localhost:50055
name: "Fred"
email: "fred@fred.com"
age: 89
Rpc succeeded with OK status
```
* add another doctor
```
grpc_cli call localhost:50055 AddDoctor "name: 'Fred' email: 'fred@fred.com' age:89"
connecting to localhost:50055
name: "Fred"
email: "fred@fred.com"
age: 89
Rpc succeeded with OK status
```
* list all the doctors
```
grpc_cli call localhost:50055 ListDoctors ""
connecting to localhost:50055
name: "Bob"
email: "bob@bob.com"
age: 98
name: "Fred"
email: "fred@fred.com"
age: 89
Rpc succeeded with OK status
```

## Local python development

* `cd server/python`
* `virtualenv venv`
* `source venv/bin/activate`
* `pip install -r requirements.txt`

## Generate stubs for Python

* make sure your local python development is setup and `virtualenv` environment is active
* `cd server/python`
* `./generate_proto.sh`


## gRPC-Web
https://github.com/grpc/grpc-web


Need eslint to ignore gRPC-web files
https://github.com/grpc/grpc-web/issues/447

```
  "eslintConfig": {
    "extends": [
      "react-app",
      "react-app/jest"
    ],
    "ignorePatterns": ["**/*_pb.js"]
  }
```