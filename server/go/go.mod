module gitlab.com/vince.fonte/grpc-example/server/go

go 1.15

require (
	github.com/bdlm/log v0.1.20
	github.com/bdlm/log/v2 v2.0.3
	github.com/bdlm/std v1.0.1 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.0.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	google.golang.org/genproto v0.0.0-20201119123407-9b1e624d6bc4
	google.golang.org/grpc v1.33.2
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.0.1
	google.golang.org/protobuf v1.25.0
)
