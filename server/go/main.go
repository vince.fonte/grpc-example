package main

import (
	"context"
	"flag"
	"net"
	"net/http"
	"sync"

	log "github.com/bdlm/log/v2"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	pb "gitlab.com/vince.fonte/grpc-example/server/go/gen"
	"gitlab.com/vince.fonte/grpc-example/server/go/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	// command-line options:
	httpURL = flag.String("HTTP_URL", ":8081", "http URL")
	grpcURL = flag.String("GRPC_URL", ":50051", "grpc URL")
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	var wg sync.WaitGroup

	// create new instance of DoctorService
	service := service.NewDoctorService()

	// Register gRPC server endpoint
	opts := []grpc.DialOption{grpc.WithInsecure()}

	// create grpc_gatway muxs
	mux := runtime.NewServeMux()

	// create new gRPC server with default options
	grpcServer := grpc.NewServer()

	// register Email Parser gRPC server
	pb.RegisterDoctorServiceServer(
		grpcServer,
		service,
	)

	log.Infof("Registering handler from endpoint: %s", *grpcURL)

	// register endpoint handler on gRPC port
	err := pb.RegisterDoctorServiceHandlerFromEndpoint(
		ctx,
		mux,
		*grpcURL,
		opts,
	)
	if err != nil {
		return err
	}

	// register reflection service on gRPC server.
	// this is used for discovering services and communicating
	// via grpc_cli. it adds very little overhead and should always be included
	log.Infof("Registering reflection server")
	reflection.Register(grpcServer)

	grpcListener, err := net.Listen("tcp", *grpcURL)
	if err != nil {
		return err
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		log.Println("starting gRPC server")
		err = grpcServer.Serve(grpcListener)
		if err != nil {
			cancel()
			log.Panicln("failed to server gRPC")
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		log.Println("starting HTTP server")
		err = http.ListenAndServe(*httpURL, mux)
		if err != nil {
			cancel()
			log.Panicln("failed to serve HTTP")
		}
	}()

	wg.Wait()

	grpcServer.GracefulStop()

	return nil
}

func main() {
	flag.Parse()

	if err := run(); err != nil {
		log.Fatal(err)
	}
}
