package service

import (
	"context"
	"time"

	"github.com/bdlm/log"
	"github.com/patrickmn/go-cache"
	pb "gitlab.com/vince.fonte/grpc-example/server/go/gen"
)

// DoctorService implements DoctorService server
type DoctorService struct {
	pb.UnimplementedDoctorServiceServer
	db *cache.Cache
}

// NewDoctorService returns a new DoctorService
func NewDoctorService() DoctorService {
	return DoctorService{
		db: cache.New(cache.NoExpiration, 10*time.Minute),
	}
}

// ListDoctors returns a list of doctors
func (svc DoctorService) ListDoctors(ctx context.Context, msg *pb.NilMsg) (*pb.DoctorList, error) {
	log.Info("getting doctors!")

	result := &pb.DoctorList{}

	// iterate through all the items in the database
	for _, item := range svc.db.Items() {
		result.Doctors = append(result.Doctors, item.Object.(*pb.Doctor))
	}

	return result, nil
}

// ListDoctorsStream streams a list of doctors
func (svc DoctorService) ListDoctorsStream(msg *pb.NilMsg, stream pb.DoctorService_ListDoctorsStreamServer) error {
	log.Info("streaming doctors!")

	// iterate through all the items in the database
	for _, item := range svc.db.Items() {
		doctor := item.Object.(*pb.Doctor)
		// stream the list of doctors until there is an error
		if err := stream.Send(doctor); err != nil {
			return err
		}
	}
	// end of stream
	return nil
}

// Alive is used to see if the service is alavilable
func (svc DoctorService) Alive(ctx context.Context, msg *pb.NilMsg) (*pb.NilMsg, error) {
	log.Info("alive check")

	return &pb.NilMsg{}, nil
}

// AddDoctor adds a doctor to the database
func (svc DoctorService) AddDoctor(ctx context.Context, msg *pb.Doctor) (*pb.Doctor, error) {
	log.Info("adding a doctor!")

	// add the doctor to the data store
	svc.db.Add(msg.GetName(), msg, cache.NoExpiration)

	// return the doctor to let the client now it was successful
	return msg, nil
}
