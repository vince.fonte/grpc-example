# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: doctors.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.api import annotations_pb2 as google_dot_api_dot_annotations__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='doctors.proto',
  package='proto',
  syntax='proto3',
  serialized_options=b'Z,gitlab.com/grpc-example/server/proto/gen;gen',
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\rdoctors.proto\x12\x05proto\x1a\x1cgoogle/api/annotations.proto\",\n\nDoctorList\x12\x1e\n\x07\x64octors\x18\x01 \x03(\x0b\x32\r.proto.Doctor\"2\n\x06\x44octor\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05\x65mail\x18\x02 \x01(\t\x12\x0b\n\x03\x61ge\x18\x03 \x01(\x05\"\x08\n\x06NilMsg2\x9f\x02\n\rDoctorService\x12\x44\n\x0bListDoctors\x12\r.proto.NilMsg\x1a\x11.proto.DoctorList\"\x13\x82\xd3\xe4\x93\x02\r\x12\x0b/v1/doctors\x12O\n\x11ListDoctorsStream\x12\r.proto.NilMsg\x1a\r.proto.Doctor\"\x1a\x82\xd3\xe4\x93\x02\x14\x12\x12/v1/doctors_stream0\x01\x12@\n\tAddDoctor\x12\r.proto.Doctor\x1a\r.proto.Doctor\"\x15\x82\xd3\xe4\x93\x02\x0f\"\n/v1/doctor:\x01*\x12\x35\n\x05\x41live\x12\r.proto.NilMsg\x1a\r.proto.NilMsg\"\x0e\x82\xd3\xe4\x93\x02\x08\x12\x06/aliveB.Z,gitlab.com/grpc-example/server/proto/gen;genb\x06proto3'
  ,
  dependencies=[google_dot_api_dot_annotations__pb2.DESCRIPTOR,])




_DOCTORLIST = _descriptor.Descriptor(
  name='DoctorList',
  full_name='proto.DoctorList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='doctors', full_name='proto.DoctorList.doctors', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=54,
  serialized_end=98,
)


_DOCTOR = _descriptor.Descriptor(
  name='Doctor',
  full_name='proto.Doctor',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='proto.Doctor.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='email', full_name='proto.Doctor.email', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='age', full_name='proto.Doctor.age', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=100,
  serialized_end=150,
)


_NILMSG = _descriptor.Descriptor(
  name='NilMsg',
  full_name='proto.NilMsg',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=152,
  serialized_end=160,
)

_DOCTORLIST.fields_by_name['doctors'].message_type = _DOCTOR
DESCRIPTOR.message_types_by_name['DoctorList'] = _DOCTORLIST
DESCRIPTOR.message_types_by_name['Doctor'] = _DOCTOR
DESCRIPTOR.message_types_by_name['NilMsg'] = _NILMSG
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

DoctorList = _reflection.GeneratedProtocolMessageType('DoctorList', (_message.Message,), {
  'DESCRIPTOR' : _DOCTORLIST,
  '__module__' : 'doctors_pb2'
  # @@protoc_insertion_point(class_scope:proto.DoctorList)
  })
_sym_db.RegisterMessage(DoctorList)

Doctor = _reflection.GeneratedProtocolMessageType('Doctor', (_message.Message,), {
  'DESCRIPTOR' : _DOCTOR,
  '__module__' : 'doctors_pb2'
  # @@protoc_insertion_point(class_scope:proto.Doctor)
  })
_sym_db.RegisterMessage(Doctor)

NilMsg = _reflection.GeneratedProtocolMessageType('NilMsg', (_message.Message,), {
  'DESCRIPTOR' : _NILMSG,
  '__module__' : 'doctors_pb2'
  # @@protoc_insertion_point(class_scope:proto.NilMsg)
  })
_sym_db.RegisterMessage(NilMsg)


DESCRIPTOR._options = None

_DOCTORSERVICE = _descriptor.ServiceDescriptor(
  name='DoctorService',
  full_name='proto.DoctorService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=163,
  serialized_end=450,
  methods=[
  _descriptor.MethodDescriptor(
    name='ListDoctors',
    full_name='proto.DoctorService.ListDoctors',
    index=0,
    containing_service=None,
    input_type=_NILMSG,
    output_type=_DOCTORLIST,
    serialized_options=b'\202\323\344\223\002\r\022\013/v1/doctors',
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='ListDoctorsStream',
    full_name='proto.DoctorService.ListDoctorsStream',
    index=1,
    containing_service=None,
    input_type=_NILMSG,
    output_type=_DOCTOR,
    serialized_options=b'\202\323\344\223\002\024\022\022/v1/doctors_stream',
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='AddDoctor',
    full_name='proto.DoctorService.AddDoctor',
    index=2,
    containing_service=None,
    input_type=_DOCTOR,
    output_type=_DOCTOR,
    serialized_options=b'\202\323\344\223\002\017\"\n/v1/doctor:\001*',
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='Alive',
    full_name='proto.DoctorService.Alive',
    index=3,
    containing_service=None,
    input_type=_NILMSG,
    output_type=_NILMSG,
    serialized_options=b'\202\323\344\223\002\010\022\006/alive',
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_DOCTORSERVICE)

DESCRIPTOR.services_by_name['DoctorService'] = _DOCTORSERVICE

# @@protoc_insertion_point(module_scope)
