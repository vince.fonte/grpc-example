#!/bin/bash

# need to use python with grpc_tools.protoc and any packages
# in requirements.txt

PROTO="../../proto/doctors.proto"

rm -rf gen/*

python3 -m grpc_tools.protoc \
   -I ../../proto \
   --python_out . \
   --grpc_python_out . \
   $PROTO
