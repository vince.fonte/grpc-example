from concurrent import futures
# add the following import statement to use server reflection
from grpc_reflection.v1alpha import reflection

import grpc
import sys

# import the protobuf generated files
import doctors_pb2
import doctors_pb2_grpc

class DoctorService(doctors_pb2_grpc.DoctorServiceServicer):
    """ a service for storing doctors """
    # this class implements all methods defined in the DoctorService
    # service defined in the protobuf. any rpc methods defined there
    # must be overwritten or else there will throw an error 

    def __init__(self):
        # list of doctors_pb2.Doctor objects
        self.doctors = []

    def Alive(self, request, context):
        """ makes sure the service is alive """
        print("alive!", file=sys.stderr)
        return doctors_pb2.NilMsg()

    def AddDoctor(self, request, context):
        """ adds a doctor to the database """
        print("adding a doc!", file=sys.stderr)
        # request is a doctors_pb2.Doctor object. since we are not manipulating it
        # we can just be added to the list
        self.doctors.append(request)
        return request

    def ListDoctorsStream(self, request, context):
        """ streams a list of doctors that have been added """
        print("streaming some docs!", file=sys.stderr)
        # stream a list of doctors_pb2.Doctor objects
        for doctor in self.doctors:
            yield doctor

    def ListDoctors(self, request, context):
        """ returns a list of doctors that have been added """
        print("getting some docs!", file=sys.stderr)
        # create a new doctors_pb2.DoctorList object which
        # holds a list of doctors_pb2.Doctors
        doctorList = doctors_pb2.DoctorList(doctors=self.doctors)
        return doctorList


def serve():
    """ serves a server """

    # create a new grpc server with a pool of threads
    print("creating server", file=sys.stderr)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    # add the DoctorService class to the grpc server, which overrides the default class
    print("adding grpc server", file=sys.stderr)
    doctors_pb2_grpc.add_DoctorServiceServicer_to_server(DoctorService(), server)

    # add the reflection server. this is used for discovering services and communicating
    # via grpc_cli. it adds very little overhead and should always be included
    print("adding reflection server", file=sys.stderr)
    # the reflection service will be aware of "DoctorService" and "ServerReflection" services.
    service_names = (
        doctors_pb2.DESCRIPTOR.services_by_name['DoctorService'].full_name,
        reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(service_names, server)

    # add the port to the grpc server
    server.add_insecure_port('[::]:50055')
    print("starting server on port 50055", file=sys.stderr)

    # start grpc server
    server.start()

    # kill on termination
    server.wait_for_termination()

if __name__ == '__main__':
    serve()