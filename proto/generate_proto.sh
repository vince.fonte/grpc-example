#!/bin/bash

PROTO="doctors.proto"

# grpc stubs
echo "creating go stubs"
rm ../server/go/gen/*
protoc -I . \
   --go_out ../server/go/gen --go_opt paths=source_relative \
   --go-grpc_out ../server/go/gen --go-grpc_opt paths=source_relative \
   $PROTO

# json proxy
echo "creating go json proxy"
protoc -I . \
   --grpc-gateway_out ../server/go/gen \
   --grpc-gateway_opt logtostderr=true \
   --grpc-gateway_opt paths=source_relative \
   $PROTO

# typescript
echo "creating gRPC-web javascript"
rm -rf ../app/src/proto/*.ts
rm -rf ../app/src/proto/*.js
protoc -I . \
   --js_out=import_style=commonjs,binary:../app/src/proto \
   --grpc-web_out=import_style=commonjs,mode=grpcwebtext:../app/src/proto \
   $PROTO

echo "creating google api javascript"
protoc -I . \
   --js_out=import_style=commonjs,binary:../app/src/proto \
   --grpc-web_out=import_style=typescript,mode=grpcwebtext:../app/src/proto \
   google/api/*

# openapi docs
echo "creating openapi docs"
rm -rf docs/*
protoc -I . \
   --openapiv2_out ./docs \
   --openapiv2_opt logtostderr=true \
   $PROTO


## Python
echo "see server/python/generate_proto.sh for generating python stubs"